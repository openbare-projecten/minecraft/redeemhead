package nl.robertvankammen.redeemhead;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static org.bukkit.Material.PLAYER_HEAD;
import static org.bukkit.Material.PLAYER_WALL_HEAD;

public class Main extends JavaPlugin implements Listener {
    private static final String COMMAND_STRING = "redeem";
    private static final Integer SPAWNRADIUS = 15;
    private static final String PLAYER_INFO_CONFIG = "data";
    private static final String WORLD_NAME = "world";
    private static final Integer COOLDOWN_HEAD_DROP_MINUTES = 30;
    private static final String LORE = "Go to spawn to /redeem your kill";

    private Hologram hologramScore;

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
        initConfig();
        initScoreboard();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase(COMMAND_STRING) && sender instanceof Player player) {
            if (inSpawn(player.getLocation())) {
                if (checkValiditemInMainHand(player.getInventory())) {
                    redeemPoint(player);
                } else {
                    player.sendMessage("You dont have a valid item in your mainhand");
                }
            } else {
                player.sendMessage("You need to be at spawn(in the overworld) to run this command");
            }
        }
        return false;
    }

    @EventHandler
    public void onKill(PlayerDeathEvent e) {
        var player = e.getEntity();
        if (player.getLastDamageCause() != null && e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent killerPlayer) {
            if (!killerPlayer.getDamager().getType().equals(EntityType.PLAYER)) {
                return;
            }
            if (killerPlayer.getDamager().getUniqueId().equals(player.getUniqueId())) {
                return;
            }
            var listConfig = (List<PlayerInfo>) getConfig().getList(PLAYER_INFO_CONFIG);
            if (listConfig == null) {
                listConfig = new ArrayList<>();
            }
            var optionalPlayerInfo = listConfig.stream().filter(playerInfo -> playerInfo.getUuid().equals(player.getUniqueId().toString())).findAny();
            if (optionalPlayerInfo.isPresent()) {
                var playerInfo = optionalPlayerInfo.get();
                if (playerInfo.getLastDeath().isAfter(LocalDateTime.now().minusMinutes(COOLDOWN_HEAD_DROP_MINUTES))) {
                    return;
                }
                listConfig.remove(playerInfo);
                playerInfo.setLastDeath(LocalDateTime.now());
                listConfig.add(playerInfo);
            } else {
                var playerInfo = new PlayerInfo(player.getUniqueId().toString(), LocalDateTime.now(), 0);
                listConfig.add(playerInfo);
            }
            getConfig().set(PLAYER_INFO_CONFIG, listConfig);
            saveConfig();
            player.getWorld().dropItem(player.getLocation(), getHead(player));
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.isCancelled()) return;
        if (event.getPlayer().isOp()) return;
        var blockPlaced = event.getBlockPlaced();

        if (blockPlaced.getType() == PLAYER_HEAD || blockPlaced.getType() == PLAYER_WALL_HEAD) {
            event.setCancelled(true);
        }

    }

    private void initConfig() {
        setDefaultConfig("pointBoardX", 0.0);
        setDefaultConfig("pointBoardY", 70.0);
        setDefaultConfig("pointBoardZ", 0.0);
        saveConfig();
    }

    private void setDefaultConfig(String location, double value) {
        if (getConfig().get(location) == null) {
            getConfig().set(location, value);
        }
    }

    private void initScoreboard() {
        var world = getServer().getWorlds().get(0);
        var locationX = (double) getConfig().get("pointBoardX");
        var locationY = (double) getConfig().get("pointBoardY");
        var locationZ = (double) getConfig().get("pointBoardZ");
        var location = new Location(world, locationX, locationY, locationZ);

        hologramScore = HologramsAPI.createHologram(this, location);

        hologramScore.insertTextLine(0, "---------------");
        hologramScore.insertTextLine(1, ChatColor.AQUA + "Score");
        updateScoreboard(hologramScore);
    }

    private void updateScoreboard(Hologram hologram) {
        var count = new AtomicInteger(0);
        for (int i = 0; i < 10; i++) {
            if (hologram.size() > 2) {
                hologram.removeLine(2);
            }
        }

        var listConfig = (List<PlayerInfo>) getConfig().getList(PLAYER_INFO_CONFIG);
        if (listConfig != null) {
            listConfig.stream()
                    .sorted(PlayerInfo::compareTo)
                    .forEach(playerInfo -> {
                        if (count.get() < 10 && playerInfo.getScore() > 0) {
                            var line = count.incrementAndGet();
                            var player = Bukkit.getOfflinePlayer(UUID.fromString(playerInfo.getUuid()));
                            hologram.insertTextLine(line + 1, ChatColor.AQUA + player.getName() + " " + ChatColor.DARK_PURPLE + playerInfo.getScore());
                        }
                    });
        }
    }


    private void redeemPoint(Player player) {
        var itemInMainHand = player.getInventory().getItemInMainHand();
        var head = (SkullMeta) itemInMainHand.getItemMeta();
        player.sendMessage("You redeemed a head of " + head.getOwningPlayer().getName());
        System.out.println("You redeemed a head of " + head.getOwningPlayer().getName());
        itemInMainHand.setAmount(itemInMainHand.getAmount() - 1);

        //config
        var listConfig = (List<PlayerInfo>) getConfig().getList(PLAYER_INFO_CONFIG);
        if (listConfig == null) {
            listConfig = new ArrayList<>();
        }
        var optionalPlayerInfo = listConfig.stream().filter(playerInfo -> playerInfo.getUuid().equals(player.getUniqueId().toString())).findAny();
        if (optionalPlayerInfo.isPresent()) {
            var playerInfo = optionalPlayerInfo.get();
            playerInfo.addOneToScore();
            listConfig.remove(playerInfo);
            playerInfo.setLastDeath(LocalDateTime.now());
            listConfig.add(playerInfo);
        } else {
            var playerInfo = new PlayerInfo(player.getUniqueId().toString(), LocalDateTime.now(), 1);
            listConfig.add(playerInfo);
        }
        getConfig().set(PLAYER_INFO_CONFIG, listConfig);
        saveConfig();
        updateScoreboard(hologramScore);
    }

    private ItemStack getHead(Player player) {
        var item = new ItemStack(Material.PLAYER_HEAD, 1);
        var skull = (SkullMeta) item.getItemMeta();
        assert skull != null;
        skull.setDisplayName(player.getName());
        var lore = new ArrayList<String>();
        lore.add(LORE);
        skull.setLore(lore);
        skull.setOwningPlayer(player);
        item.setItemMeta(skull);
        return item;
    }

    private boolean inSpawn(Location location) {
        return ((location.getBlockX() <= SPAWNRADIUS && location.getBlockX() >= -SPAWNRADIUS) &&
                location.getBlockZ() <= SPAWNRADIUS && location.getBlockZ() >= -SPAWNRADIUS) && location.getWorld().getName().equals(WORLD_NAME);
    }

    private boolean checkValiditemInMainHand(PlayerInventory inventory) {
        var itemInMainHand = inventory.getItemInMainHand();
        if (itemInMainHand.getType().equals(Material.PLAYER_HEAD)) {
            var head = (SkullMeta) itemInMainHand.getItemMeta();
            if (head != null && head.getLore() != null && head.getLore().contains(LORE)) {
                return true;
            }
        }

        return false;
    }
}
