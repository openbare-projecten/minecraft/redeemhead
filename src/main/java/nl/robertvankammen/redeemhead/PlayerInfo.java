package nl.robertvankammen.redeemhead;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class PlayerInfo implements ConfigurationSerializable {
    static {
        ConfigurationSerialization.registerClass(PlayerInfo.class);
    }

    private String uuid;
    private String lastDeath;
    private Integer score;

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
        data.put("last_death", lastDeath);
        data.put("uuid", uuid);
        data.put("score", score);
        return data;
    }

    public PlayerInfo(String uuid, LocalDateTime lastDeath, Integer score) {
        this.uuid = uuid;
        setLastDeath(lastDeath);
        this.score = score;
    }

    public void addOneToScore() {
        this.score++;
    }

    public PlayerInfo(Map<String, Object> data) {
        lastDeath = (String) data.get("last_death");
        uuid = (String) data.get("uuid");
        score = (Integer) data.get("score");
    }

    public LocalDateTime getLastDeath() {
        return LocalDateTime.parse(lastDeath);
    }

    public void setLastDeath(LocalDateTime lastDeath) {
        this.lastDeath = lastDeath.toString();
    }

    public String getUuid() {
        return uuid;
    }

    public Integer getScore() {
        return score;
    }

    public int compareTo(PlayerInfo playerScore) {
        return playerScore.getScore() - this.score;
    }

}
